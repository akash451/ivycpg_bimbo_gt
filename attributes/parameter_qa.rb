get_cookbook_name = "cookbook_name"
env = 'qa'

#Server Physical Paths
default[get_cookbook_name]['parameter_' + env]['iis_path'] = "/var/www"
default[get_cookbook_name]['parameter_' + env]['download_path'] = "/home/node/IvyDnB/downloads"
default[get_cookbook_name]['parameter_' + env]['deploy_path'] = "/home/node/IvyDnB/deployment"
default[get_cookbook_name]['parameter_' + env]['backup_path'] = "/home/node/IvyDnB/backup"

default[get_cookbook_name]['parameter_' + env]['site_name'] = "bimbo-gt-qa.ivycpg.com"