require 'aws-sdk-s3'      
require 'fileutils'
require 'zip'

get_cookbook_name = "cookbook_name"
env = node.chef_environment
time = Time.now.strftime("%Y_%m_%d_%H_%M")

cred = chef_vault_item("secrets", "aws")
s3 = Aws::S3::Client.new(           #s3 credentials
  region: cred['region'],
  access_key_id: cred['access_key'],
  secret_access_key: cred['secret_access_key']
)

#paths to respective folders
build_name = "#{node[get_cookbook_name]['s3_path']['web']}"+"#{node[get_cookbook_name]['revision_no']['web']}"+".zip"
download_path = "#{node[get_cookbook_name]['parameter_' + env]['download_path']}/#{node[get_cookbook_name]['parameter_' + env]['site_name']}/web/#{time}/#{node[get_cookbook_name]['revision_no']['web']}.zip"
backup_path = "#{node[get_cookbook_name]['parameter_' + env]['backup_path']}/#{node[get_cookbook_name]['parameter_' + env]['site_name']}/web/#{time}/"
deploy_path = "#{node[get_cookbook_name]['parameter_' + env]['deploy_path']}/#{node[get_cookbook_name]['parameter_' + env]['site_name']}/web/#{time}/"
iis_path = "#{node[get_cookbook_name]['parameter_' + env]['iis_path']}/#{node[get_cookbook_name]['parameter_' + env]['site_name']}/web/"

#create directory if not there
FileUtils.mkdir_p(File.dirname(download_path))
FileUtils.mkdir_p(backup_path)
FileUtils.mkdir_p(File.dirname(deploy_path))
FileUtils.mkdir_p(File.dirname(iis_path))

# Copy contents excluding log files from iis to backup folder
FileUtils.cp_r(Dir.glob("#{iis_path}/**/*").reject { |d| d.include?('Logs') }, backup_path)

# Delete everything except logs from iis
Dir.glob("#{iis_path}/**/*").reject { |d| d.include?('Logs') }.each do |entry|
  FileUtils.rm_rf(entry)
end

# Create a ZIP archive of backup
Zip::File.open(backup_path  + "#{node[get_cookbook_name]['revision_no']['web']}"+".zip", Zip::File::CREATE) do |zipfile|
  Dir[File.join(backup_path, '**', '**')].each do |file|
    relative_path = file.sub("#{iis_path}/", '')
    relative_path = relative_path.sub(/^\//, '')
    zipfile.add(relative_path, file)
  end
end

#deleting everthing in backup folder except .zip file
entries = Dir.entries(backup_path)

entries.each do |entry|
  next if entry == '.' || entry == '..' || entry.end_with?('.zip')

  entry_path = File.join(backup_path, entry)

  if File.directory?(entry_path)
    FileUtils.rm_rf(entry_path)  # Recursively delete folder
  else
    File.delete(entry_path)  # Delete file
  end
end

#dowmloading build from s3
s3.get_object(bucket: cred['bucket_name'], key: build_name, response_target: download_path)

#deploying to the deployment path
Zip::File.open(download_path) do |zip_file|
   zip_file.each do |entry|
     entry_path = File.join(deploy_path, entry.name)
     FileUtils.mkdir_p(File.dirname(entry_path))
     entry.extract(entry_path)
   end
end

#copying files from deploy path to /var/www/
FileUtils.cp_r("#{deploy_path}/.", iis_path)
