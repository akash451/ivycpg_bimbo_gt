require 'aws-sdk-s3' 
get_cookbook_name = "ivycpg_bimbo_gt"
env = "qa"
time = Time.now.strftime("%Y_%m_%d_%H_%M")

cred = chef_vault_item("secrets", "aws")

s3 = Aws::S3::Client.new(           #s3 credentials
  region: cred['region'],
  access_key_id: cred['access_key'],
  secret_access_key: cred['secret_access_key']
)

build_name = "#{node[get_cookbook_name]['s3_path']['web']}"+"#{node[get_cookbook_name]['revision_no']['web']}"+".zip"
puts build_name
download_path = "#{node[get_cookbook_name]['parameter_' + env]['download_path']}/#{node[get_cookbook_name]['parameter_' + env]['site_name']}/web/#{time}/#{node[get_cookbook_name]['revision_no']['web']}.zip"
puts download_path

FileUtils.mkdir_p(File.dirname(download_path))


s3.get_object(bucket: 'ivy-devops-build1', key: build_name, response_target: download_path)
